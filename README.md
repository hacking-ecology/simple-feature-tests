# Nayad Main Tests

This document aims to provide basic tests for Nayad v1.0 board.

This document covers:

- [Flash test and serial communication](#id-flash)
- [LoRa Communication: Receiver and Transmiter](#id-LoRa)
- [GPS test](#id-GPS)

--------------

### Using Terminal

<div id='id-flash'/>

### Testing using esptool intergface (Basic Flash/Serial Communication)
- Install software https://docs.espressif.com/projects/esptool/en/latest/esp32/
- Plug the USB board
- Open terminal and type:

```python -m esptool flash_id```


### Expected output (the port and MAC might be different):

```Found 1 serial ports
Serial port /dev/ttyUSB0
Connecting....
Detecting chip type... Unsupported detection protocol, switching and trying again...
Connecting......
Detecting chip type... ESP32
Chip is ESP32-D0WD-V3 (revision v3.0)
Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
Crystal is 40MHz
MAC: 94:e6:86:9a:f7:90
Uploading stub...
Running stub...
Stub running...
Manufacturer: 20
Device: 4018
Detected flash size: 16MB
Hard resetting via RTS pin...
```


### Error / Warning outputs (the port and MAC might be different):

#### Possibility 1: WARNING: Failed to communicate with the flash chip, read/write operations will fail. Try checking the chip connections or removing any other hardware connected to IOs.
```
esptool.py v4.6.2
Found 1 serial ports
Serial port /dev/ttyUSB0
Connecting....
Detecting chip type... Unsupported detection protocol, switching and trying again...
Connecting....
Detecting chip type... ESP32
Chip is ESP32-D0WD-V3 (revision v3.0)
Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
Crystal is 40MHz
MAC: 94:e6:86:9b:2b:34
Uploading stub...
Running stub...
Stub running...
WARNING: Failed to communicate with the flash chip, read/write operations will fail. Try checking the chip connections or removing any other hardware connected to IOs.
Manufacturer: 00
Device: 0000
Detected flash size: Unknown
Hard resetting via RTS pin...
```

#### Possibility 2: A fatal error occurred: Could not connect to an Espressif device on any of the 1 available serial ports.

```
esptool.py v4.6.2
Found 1 serial ports
Serial port /dev/ttyUSB0
Connecting......................................
/dev/ttyUSB0 failed to connect: Failed to connect to Espressif device: Invalid head of packet (0x00): Possible serial noise or corruption.
For troubleshooting steps visit: https://docs.espressif.com/projects/esptool/en/latest/troubleshooting.html

A fatal error occurred: Could not connect to an Espressif device on any of the 1 available serial ports.
```


## Using Arduino IDE
Download Arduino IDE: https://www.arduino.cc/en/software

<div id='id-LoRa'/>

### Testing LoRa module (SX1262):

- Platform: Arduino IDE v2
- Library:
	- RadioLib: https://www.arduino.cc/reference/en/libraries/radiolib/

- 2 boards needed: 1 receiver, 1 transmiter
- Codes: 

### Transmiter
- Code: [LoRa Transmiter](https://gitlab.com/hacking-ecology/simple-feature-tests/-/blob/main/firmware_test/Transmit-v3/Transmit-v3.ino)

#### Expected output:

```
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	32000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	48000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	32000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	48000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	32000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	48000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	32000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	48000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	32000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	48000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	32000000.00 bps
[SX1262] Transmitting packet ... success!
[SX1262] Datarate:	48000000.00 bps
```

#### Error

```
[SX1262] Initializing ... failed, code -2
```


### Receiver
- Code: [LoRa Receiver](https://gitlab.com/hacking-ecology/simple-feature-tests/-/blob/main/firmware_test/Receive-v3/Receive-v3.ino)

#### Expected output
```
[SX1262] RSSI:		-38.00 dBm
[SX1262] SNR:		13.25 dB
[SX1262] Waiting for incoming transmission ... timeout!
[SX1262] Waiting for incoming transmission ... timeout!
0[SX1262] Waiting for incoming transmission ... success!
[SX1262] Data:		Hello World!
0[SX1262] RSSI:		-39.00 dBm
[SX1262] SNR:		12.50 dB
[SX1262] Waiting for incoming transmission ... timeout!
[SX1262] Waiting for incoming transmission ... timeout!
0[SX1262] Waiting for incoming transmission ... success!
[SX1262] Data:		Hello World!
0[SX1262] RSSI:		-39.00 dBm
[SX1262] SNR:		12.25 dB
[SX1262] Waiting for incoming transmission ... timeout!
[SX1262] Waiting for incoming transmission ... timeout!
0[SX1262] Waiting for incoming transmission ... success!
```

#### Error1

```
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
[SX1262] Waiting for incoming transmission ... failed, code -705
```

<div id='id-GPS'/>

## Testing GPS (ATGM336H-5N31):
- Platform: Arduino IDE
- Libraries: 
	- TinyGPSPlus: https://www.arduino.cc/reference/en/libraries/tinygpsplus/
	- SoftwareSerial: https://docs.arduino.cc/learn/built-in-libraries/software-serial

- Code: [GPS Full](https://gitlab.com/hacking-ecology/simple-feature-tests/-/blob/main/firmware_test/GPS_NAYAD_FullExample/GPS_NAYAD_FullExample.ino)

## Output (Before GNSSS Satellite connection)

```
23:11:04.412 -> An extensive example of many interesting TinyGPSPlus features
23:11:04.412 -> Testing TinyGPSPlus library v. 1.0.2
23:11:04.412 -> by Mikal Hart
23:11:04.412 -> 
23:11:04.412 -> Sats HDOP  Latitude   Longitude   Fix  Date       Time     Date Alt    Course Speed Card  Distance Course Card  Chars Sentences Checksum
23:11:04.444 ->            (deg)      (deg)       Age                      Age  (m)    --- from GPS ----  ---- to London  ----  RX    RX        Fail
23:11:04.444 -> ----------------------------------------------------------------------------------------------------------------------------------------
23:11:04.444 -> **** ***** ********** *********** **** ********** ******** **** ****** ****** ***** ***   ******** ****** ***   0     0         0        
23:11:05.442 -> 0    25.5  ********** *********** **** 10/31/2023 22:11:05 180  ****** ****** ***** ***   ******** ****** ***   332   0         0        
23:11:06.472 -> 0    25.5  ********** *********** **** 10/31/2023 22:11:06 184  ****** ****** ***** ***   ******** ****** ***   664   0         0        
23:11:07.471 -> 0    25.5  ********** *********** **** 10/31/2023 22:11:07 188  ****** ****** ***** ***   ******** ****** ***   996   0         0        
23:11:08.468 -> 0    25.5  ********** *********** **** 10/31/2023 22:11:08 192  ****** ****** ***** ***   ******** ****** ***   1328  0         0        
23:11:06.472 -> 0    25.5  ********** *********** **** 10/31/2023 22:11:06 184  ****** ****** ***** ***   ******** ****** ***   664   0         0        
23:11:07.471 -> 0    25.5  ********** *********** **** 10/31/2023 22:11:07 188  ****** ****** ***** ***   ******** ****** ***   996   0         0        
23:11:08.468 -> 0    25.5  ********** *********** **** 10/31/2023 22:11:08 192  ****** ****** ***** ***   ******** ****** ***   1328  0         0        
23:11:09.467 -> 0    25.5  ********** *********** **** 10/31/2023 22:11:09 196  ****** ****** ***** ***   ******** ****** ***   1660  0         0 
```

## Output After GNSS Satellite connection

```
11:52:45.094 -> 9    1.2   40.711548  0.577654    92   11/01/2023 10:52:45 893  86.10  ****** ***** ***   1202     357.66 N     1590  1         0        
11:52:46.088 -> 9    1.2   40.711552  0.577653    100  11/01/2023 10:52:45 572  86.10  0.00   1.30  N     1202     357.66 N     2283  3         0        
11:52:47.080 -> 9    1.2   40.711552  0.577654    116  11/01/2023 10:52:47 580  86.10  0.00   1.11  N     1202     357.66 N     2983  5         0        
11:52:48.095 -> 10   1.1   40.711552  0.577655    116  11/01/2023 10:52:48 596  85.90  0.00   1.02  N     1202     357.66 N     3668  7         0        
11:52:49.108 -> 10   1.1   40.711555  0.577654    130  11/01/2023 10:52:49 591  85.60  0.00   0.67  N     1202     357.66 N     4371  9         0        
11:52:50.095 -> 11   1.0   40.711559  0.577651    123  11/01/2023 10:52:49 605  85.40  0.00   0.06  N     1202     357.66 N     5053  11        0        
11:52:51.111 -> 11   1.0   40.711563  0.577647    131  11/01/2023 10:52:51 595  85.30  0.00   0.59  N     1202     357.66 N     5754  13        0        
11:52:52.266 -> 11   1.0   40.711567  0.577641    135  11/01/2023 10:52:52 603  85.00  0.00   0.85  N     1202     357.66 N     6451  15        0        
11:52:53.147 -> 11   1.0   40.711571  0.577636    147  11/01/2023 10:52:53 607  84.80  0.00   0.67  N     1202     357.66 N     7155  17        0        
11:52:54.133 -> 11   1.0   40.711575  0.577625    119  11/01/2023 10:52:54 619  84.00  0.00   0.20  N     1202     357.66 N     7821  19        0        
11:52:55.150 -> 11   1.0   40.711575  0.577619    118  11/01/2023 10:52:55 591  83.80  0.00   0.06  N     1202     357.66 N     8513  21        0        
11:52:56.128 -> 11   1.0   40.711578  0.577616    125  11/01/2023 10:52:56 589  83.80  0.00   0.30  N     1202     357.66 N     9213  23        0        
11:52:57.144 -> 11   1.0   40.711582  0.577613    140  11/01/2023 10:52:57 597  83.80  0.00   0.24  N     1202     357.66 N     9920  25        0        
11:52:58.139 -> 11   1.0   40.711582  0.577611    143  11/01/2023 10:52:58 611  83.70  0.00   0.00  N     1202     357.66 N     10616 27        0        
11:52:59.151 -> 11   1.0   40.711586  0.577609    143  11/01/2023 10:52:59 615  83.60  0.00   0.00  N     1202     357.66 N     11310 29        0        
11:53:00.150 -> 11   1.0   40.711586  0.577607    144  11/01/2023 10:53:00 615  83.80  0.00   0.00  N     1202     357.66 N     12003 31        0        
11:53:01.158 -> 11   1.0   40.711586  0.577605    143  11/01/2023 10:53:01 616  83.90  0.00   0.00  N     1202     357.66 N     12695 33        0        
11:53:02.167 -> 11   1.0   40.711586  0.577604    152  11/01/2023 10:53:02 615  84.00  0.00   0.00  N     1202     357.66 N     13397 35        0        
11:53:03.180 -> 11   1.0   40.711590  0.577603    163  11/01/2023 10:53:03 624  84.10  0.00   0.00  N     1202     357.66 N     14100 37        0        
11:53:04.218 -> 12   1.0   40.711590  0.577602    162  11/01/2023 10:53:04 634  84.10  0.00   0.00  N     1202     357.66 N     14793 39        0        
11:53:05.215 -> 12   1.0   40.711590  0.577602    163  11/01/2023 10:53:05 630  84.10  0.00   0.00  N     1202     357.66 N     15491 41        0        
11:53:06.221 -> 12   1.0   40.711590  0.577601    157  11/01/2023 10:53:06 631  84.10  0.00   0.00  N     1202     357.66 N     16182 43        0
```
